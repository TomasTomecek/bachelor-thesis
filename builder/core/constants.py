"""
:::::::::::::
!!!LICENCE!!!
:::::::::::::

Constants used by builder.
"""

CONFIG_LAYERS = "layers"
CONFIG_VOLUMES = "volumes"
CONFIG_METADATA = "metadata"
CONFIG_VERSION = "version"
CONFIG_INFINITE_COMMAND = "infinite_command"
CONFIG_REPOSITORY = "repository"
CONFIG_TAGS = "tags"
CONFIG_MKDIR_COMMAND = 'mkdir_command'

DEFAULT_REPOSITORY = "builder"
DEFAULT_TAGS = ["latest"]

INFINITE_COMMAND = "cat -"
MKDIR_COMMAND = 'mkdir --parent'

DEFAULT_VERSION = 1
COMMIT_MESSAGE_START = "This layer is composed of these commands: "

FILE_MODE_FROM_URL = 0o600
