class DockerClientUnavailable(Exception):
    """
    Cannot establish connection to docker client.
    """