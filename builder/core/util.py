"""
:::::::::::::
!!!LICENCE!!!
:::::::::::::

Utility methods for builder.
"""

# python2/3 compatibility
import six

import glob
import logging
import os
import tarfile
import validators

# local imports
from builder.core.constants import FILE_MODE_FROM_URL

# Logging
logger = logging.getLogger(__name__)


def change_tarinfo_properties(tarinfo):
    """
    Filter for insertion local file to tar archive.
    :param tarinfo: tarinfo to insert
    :return: original tarinfo with changed permissions
    """
    tarinfo.uid = 0
    tarinfo.gid = 0
    return tarinfo


def change_tarinfo_properties_mode(tarinfo):
    """
    Filter for insertion not-local file to tar archive.
    :param tarinfo: tarinfo to insert
    :return: original tarinfo with changed permissions
    """
    tarinfo.uid = 0
    tarinfo.gid = 0
    tarinfo.mode = FILE_MODE_FROM_URL
    return tarinfo


def get_tars_from_source(source_paths):
    """
    Make list of tar files of given source path
    :param source_paths: source path (with bash wildcards)
    :return: list of contents of tar files
    """
    tars = []

    files = []
    remote_files_to_tar = []
    for source in source_paths:
        if validators.url(source):
            # remote_files_to_tar.append('')
            logger.debug("downloading file {}".format(source))
            continue
        else:
            for file_name in glob.glob(source):
                files.append(file_name)

    files_to_tar = []

    for f in files:

        if os.path.isdir(f):
            logger.debug("putting content of directory {} into archive".format(f))
            for file_in_directory in os.listdir(f):
                files_to_tar.append((os.path.join(f, file_in_directory), file_in_directory))

        elif tarfile.is_tarfile(f):

            logger.debug("appending source archive {}".format(f))
            with open(f, 'rb') as tar:
                tars.append(tar.readall())

        else:
            files_to_tar.append((f, f))

    if len(files_to_tar) > 0:
        in_memory_tar_file = six.BytesIO()

        with tarfile.open(mode="w", fileobj=in_memory_tar_file) as tar_file:

            for file_name, file_arcname in files_to_tar:
                logger.debug('putting file {} into archive.'.format(str(file_name)))
                tar_file.add(name=file_name,
                             arcname=file_arcname,
                             filter=change_tarinfo_properties)

            for file_name in remote_files_to_tar:
                logger.debug('putting remote file {} into archive.'.format(str(file_name)))
                tar_file.add(name=file_name,
                             arcname=file_name,
                             filter=change_tarinfo_properties_mode)

        tars.append(in_memory_tar_file.getvalue())
        in_memory_tar_file.close()

    return tars
