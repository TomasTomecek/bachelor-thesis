"""
:::::::::::::
!!!LICENCE!!!
:::::::::::::

Classes for managing configuration.
"""

import jsonschema

# local imports
from builder.core.constants import CONFIG_METADATA, CONFIG_VOLUMES, CONFIG_LAYERS, CONFIG_VERSION, \
    CONFIG_INFINITE_COMMAND, \
    INFINITE_COMMAND, DEFAULT_VERSION, COMMIT_MESSAGE_START, CONFIG_REPOSITORY, CONFIG_TAGS, DEFAULT_REPOSITORY, \
    DEFAULT_TAGS, CONFIG_MKDIR_COMMAND, MKDIR_COMMAND

schema_v1 = {
    "type": "object",
    "properties": {
        CONFIG_VERSION: {"type": "number"},
        CONFIG_LAYERS: {
            "type": "array",
            "items": {
                "type": "integer",
                "minimum": 1
            },
        },
        CONFIG_VOLUMES: {
            "properties": {},
            "patternProperties": {
                "": {
                    "type": "string"
                }
            }
        },
        CONFIG_METADATA: {
            "properties": {},
            "patternProperties": {
                "": {
                    "type": "string"
                }
            }
        },
        CONFIG_INFINITE_COMMAND: {"type": "string"},
        CONFIG_MKDIR_COMMAND: {'type': 'string'},
        CONFIG_REPOSITORY: {"type": "string"},
        CONFIG_TAGS: {
            "type": "array",
            "items": {
                "type": "string"
            }
        }
    },
}

schema = {1.0: schema_v1}


class ImageConfig:
    """
    Representation of config file.
    Structure of file is described and validated by jsonschema.
    """

    def __init__(self, config_file,
                 layers=None,
                 secret_volumes=None,
                 metadata=None,
                 infinite_command=None,
                 mkdir_command=None
                 ):

        if metadata is None:
            metadata = {}
        if secret_volumes is None:
            secret_volumes = {}
        if layers is None:
            layers = []

        config_file.setdefault(CONFIG_VERSION, DEFAULT_VERSION)

        config_file.setdefault(CONFIG_LAYERS, [])
        config_file[CONFIG_LAYERS] = list(set(config_file[CONFIG_LAYERS] + layers))
        config_file[CONFIG_LAYERS].sort()

        if 0 in config_file[CONFIG_LAYERS]:
            config_file[CONFIG_LAYERS].remove(0)

        config_file.setdefault(CONFIG_VOLUMES, {})
        config_file[CONFIG_VOLUMES].update(secret_volumes)

        config_file.setdefault(CONFIG_METADATA, {})
        config_file[CONFIG_METADATA].update(metadata)

        config_file.setdefault(CONFIG_INFINITE_COMMAND, INFINITE_COMMAND)
        if infinite_command is not None:
            config_file[CONFIG_INFINITE_COMMAND] = infinite_command

        config_file.setdefault(CONFIG_MKDIR_COMMAND, MKDIR_COMMAND)
        if mkdir_command is not None:
            config_file[CONFIG_MKDIR_COMMAND] = mkdir_command

        self.__config = config_file

    def config(self):
        """

        :return: config file as a dictionary
        """
        return self.__config

    def layers(self):
        """

        :return: list with positions for commits
            (between which commands shout be new layer)
        """
        return self.__config.get(CONFIG_LAYERS, [])

    def volumes(self):
        """

        :return: Tuples with source:destination volume only for build.
        """
        return self.__config.get(CONFIG_VOLUMES, [])

    def metadata(self):
        """

        :return: Metadata tuples, which should be added to image.
        """
        return self.__config.get(CONFIG_METADATA, [])

    def is_valid(self):
        """

        :return: True if the json structure is valid, else False.
        """
        try:
            self.validate()
            return True
        except jsonschema.exceptions.ValidationError:
            return False

    def validate(self):
        """

        :return: None if the structure of json is valid, else throws exceptions.
        """
        version = self.__config.get(CONFIG_VERSION, None)
        version_schema = schema.get(version, None)

        if version_schema is not None:
            jsonschema.validate(self.__config, schema)
            return None
        else:
            raise jsonschema.exceptions.ValidationError("Unknown config version.")

    def version(self):
        """

        :return: Version of structure from json.
        """
        return self.__config.get(CONFIG_VERSION, DEFAULT_VERSION)

    def infinite_command(self):
        return self.__config.get(CONFIG_INFINITE_COMMAND, INFINITE_COMMAND)

    def mkdir_command(self):
        return self.__config.get(CONFIG_MKDIR_COMMAND, MKDIR_COMMAND)

    def repository(self):
        return self.__config.get(CONFIG_REPOSITORY, DEFAULT_REPOSITORY)

    def tags(self):
        return self.__config.get(CONFIG_TAGS, DEFAULT_TAGS)


class LayerConfig:
    """
    Class managing commit config  and other properties for each layer commit.
    """

    def __init__(self, layer_number=None):
        if layer_number is None:
            self.layer_number = 0
        else:
            self.layer_number = layer_number
        self.commit_config = {}
        self.commands = []
        self.author = ""
        self.is_data_changed = False

    def add_property(self, key, value):
        self.commit_config[key] = value

    def add_to_dictionary(self, dictionary_name, key, value):
        self.commit_config.setdefault(dictionary_name, {})
        self.commit_config[dictionary_name][key] = value

    def add_command(self, command):
        label_name = "layer_" + str(self.layer_number) + "_commands"
        self.commit_config.setdefault("Labels", {})
        self.commit_config["Labels"].setdefault(label_name, "")
        self.commit_config["Labels"][label_name] += command
        self.commands.append(command)

    def add_labels(self, labels):
        self.commit_config.setdefault("Labels", {})
        self.commit_config["Labels"].update(labels)

    def add_envs(self, envs):
        self.commit_config.setdefault("Env", [])
        for k, v in envs.items():
            self.commit_config["Env"].append('{}:{}'.format(str(k), str(v)))
        unique_envs = list(set(self.commit_config["Env"]))
        self.commit_config["Env"] = unique_envs

    def clear_and_get_next(self):
        self.commit_config = {}
        self.layer_number += 1
        self.commands = []
        self.is_data_changed = False

    def commit_message(self):
        message = COMMIT_MESSAGE_START
        for command in self.commands:
            message += command
        return message
