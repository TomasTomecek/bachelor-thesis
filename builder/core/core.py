"""
:::::::::::::
!!!LICENCE!!!
:::::::::::::

Core functionality of builder.
"""

# python2/3 compatibility
import six

import os
import docker
from dockerfile_parse.parser import DockerfileParser

# local imports
from builder.core.output import Output
from builder.core.caching import ImageCache
from builder.core.exceptions import DockerClientUnavailable
from builder.core.config import ImageConfig, LayerConfig
from builder.core.util import get_tars_from_source


class Builder:
    """
    Class for building docker images.
    """

    def __init__(self, dockerfile, config=None, output=None):
        if config is None:
            config = ImageConfig({})

        if output is None:
            self.output = Output([])
        else:
            self.output = output

        df = six.StringIO(dockerfile)
        self._dfp = DockerfileParser(fileobj=df,
                                     cache_content=True,
                                     env_replace=False)

        self._config = config
        self._history = []
        self._initialize_state()
        self._establish_client()

    def _initialize_state(self):
        self._current_container = None
        self._last_image = None
        self._layer_config = LayerConfig()
        self._cache = ImageCache()

    def build(self):
        """
        Builds a image from given Dockerfile and configuration.
        :return: dictionary with info about final image:
            {id : "final_container_id"}
        """
        self._initialize_state()
        self._prebuild()

        current_command = 0
        self._create_base_container()
        self._mount_temporary_volumes()
        for layer_split in self._config.layers() + [len(self._dfp.structure)]:

            # Run all commands from current layer.
            for i in range(current_command, layer_split):
                self._run_command(self._dfp.structure[i])
                current_command += 1

            self._commit_container(is_last=layer_split == len(self._dfp.structure))

        self._postbuild()

        return self._last_image

    def _create_base_container(self):
        """
        Creates a base container filled in Dockerfile.
        """
        self.output.debug("create base image")
        image = self._dfp.baseimage
        command = self._config.infinite_command()
        self.output.debug('base image: {}; command: {},'.format(image, command))
        base = self.client.containers.create(image=image,
                                            command=command,
                                            stdin_open=True)
        base.start()
        self._current_container = base

    def _create_new_layer(self):
        self.output.debug("creating new layer number: {}".format(str(self._layer_config.layer_number)))
        self._mount_temporary_volumes()
        self._current_container = self.client.containers.create(
            image=self._last_image,
            command=self._config.infinite_command(),
            stdin_open=True)

        self._current_container.start()

    def _run_command(self, command):
        """
        Runs a Dockerfile command.
        :param command: Dictionary discribing command (from dockerfile_parse)
        """
        instruction = command["instruction"]
        self._layer_config.add_command(command["content"])

        self.output.debug('executing command: "{}"'.format(str(command["content"]).rstrip()))

        if instruction == "RUN":
            self._layer_config.is_data_changed = True
            self._current_container.exec_run(cmd=command["value"])
            #output?

        elif instruction == "EXPOSE":
            self._layer_config.add_to_dictionary("ExposedPorts", command["value"], {})

        elif instruction == "CMD":
            self._layer_config.add_property("Cmd", command["value"])

        elif instruction == "USER":
            self._layer_config.add_property("User", command["value"])

        elif instruction == "WORKDIR":
            self._layer_config.add_property("WorkingDir", command["value"])

        elif instruction == "MAINTAINER":
            self._layer_config.author = command["value"]

        elif instruction == "LABEL":
            pass
            #labels = self._dfp.labels_one_command(command)
            #self._layer_config.add_labels(labels)

        elif instruction == "ENV":
            pass
            #envs = self._dfp.envs_one_command(command)
            #self._layer_config.add_envs(envs)

        elif instruction == "ADD":
            self._layer_config.is_data_changed = True
            self._add_files(command)

        elif instruction == "FROM":
            pass
        else:
            print(command["instruction"])

    def _commit_container(self, is_last):
        """
        Commits a container to new image and save image id for caching.
        """

        if not (is_last or self._layer_config.is_data_changed):
            self.output.info("no data changed - skipping commit")
            return

        self._current_container.kill()
        if is_last:
            self.output.debug("committing last layer")
            image = self._current_container.commit(repository=self._config.repository(),
                                       conf=self._layer_config.commit_config,
                                       author=self._layer_config.author,
                                       message=self._layer_config.commit_message())

            for tag in self._config.tags():
                image.tag(self._config.repository(), tag)
        else:
            self.output.debug("committing middle layer")
            image = self._current_container.commit(conf=self._layer_config.commit_config,
                                       author=self._layer_config.author,
                                       message=self._layer_config.commit_message())

        self._cache.add(self._last_image, image.id, self._layer_config.commands)
        self._last_image = image
        self._current_container.remove()
        self._layer_config.clear_and_get_next()
        if not is_last:
            self._create_new_layer()

    def _prebuild(self):
        """
        For future use.
        """
        self.output.debug('prebuild')

    def _postbuild(self):
        """
        For future use.
        """
        self.output.debug('postbuild')

    def _mount_temporary_volumes(self):
        self.output.debug('mount temporary volumes')

    def _establish_client(self):
        """
        Establishes docker client from environment variables.
        """
        self.output.debug("establishing client")
        self.client = docker.from_env()

    def _add_files(self, command):
        splited_command = command['value'].split()
        if len(splited_command) < 2:
            self.output.warning('Wrong command: {}'.format(str(command['value'])))
            return
            # exception?

        source_files = splited_command[0:-1:]
        target_path = splited_command[1]

        if target_path[-1] != os.path.sep:  # destination is file
            pass
        else:  # destination is folder

            tars = get_tars_from_source(source_files)

            if len(tars) == 0:
                self.output.debug('no files to add - skipping put_archive.')
            else:

                index_of_archive = 1
                for tar in tars:
                    self.output.debug('creating target folder: {}'.format(target_path))
                    command_id = self.client.exec_create(container=self._current_container,
                                                         cmd=self._config.mkdir_command() + ' {}'.format(
                                                             target_path)).get('Id')
                    self.client.exec_start(exec_id=command_id)

                    self.output.debug('put archive {}/{} with {} into container'.format(str(index_of_archive),
                                                                                        str(len(tars)),
                                                                                        source_files))
                    self.client.put_archive(container=self._current_container,
                                            path=target_path,
                                            data=tar)
