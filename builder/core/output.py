"""
:::::::::::::
!!!LICENCE!!!
:::::::::::::

Classes for resolving output and logging.
"""

import logging
import sys

# logger of core class
logger = logging.getLogger('builder')


class Output:
    def __init__(self, *outputs):
        self.outputs = list(outputs)

    def debug(self, message):
        logger.debug(message)
        for out in self.outputs:
            try:
                out.debug(message)
            except:
                continue

    def info(self, message):
        logger.info(message)
        for out in self.outputs:
            try:
                out.info(message)
            except:
                continue

    def warning(self, message):
        logger.warning(message)
        for out in self.outputs:
            try:
                out.warning(message)
            except:
                continue

    def error(self, message):
        logger.error(message)
        for out in self.outputs:
            try:
                out.error(message)
            except:
                continue

    def critical(self, message):
        logger.critical(message)
        for out in self.outputs:
            try:
                out.critical(message)
            except:
                continue

    def progress_bar(self, collection):
        for out in self.outputs:
            try:
                return out.progress_bar(collection)
            except:
                continue
        return collection


class StandardIO:
    def __init__(self, enable_debug=False,
                 enable_info=False,
                 enable_warning=True,
                 enable_error=True,
                 enable_critical=True):
        self.enable_debug = enable_debug
        self.enable_info = enable_info
        self.enable_info = enable_info
        self.enable_warning = enable_warning
        self.enable_error = enable_error
        self.enable_critical = enable_critical

    def debug(self, message):
        if self.enable_debug:
            sys.stdout.write(message + '\n')

    def info(self, message):
        if self.enable_info:
            sys.stdout.write(message + '\n')

    def warning(self, messaage):
        if self.enable_warning:
            sys.stdout.write(messaage + '\n')

    def error(self, message):
        if self.enable_error:
            sys.stderr.write(message + '\n')

    def critical(self, message):
        if self.enable_critical:
            sys.stderr(message + '\n')
