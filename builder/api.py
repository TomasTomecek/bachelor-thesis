"""
:::::::::::::
!!!LICENCE!!!
:::::::::::::


Python API for the alternative docker builder.
"""

# local imports
from builder.core.output import Output
from builder.core.core import Builder
from builder.core.config import ImageConfig


def create_image(dockerfile,
                 config_file=None,
                 layers=None,
                 secret_volumes=None,
                 metadata=None,
                 infinite_command=None,
                 output=None
                 ):
    """
    Creates image from dockerfile content and other configuration.
    :param dockerfile:
    :param config_file:
    :param layers:
    :param secret_volumes:
    :param metadata:
    :param infinite_command:
    :param output:
    :return:
    """
    if config_file is None:
        config_file = {}
    if layers is None:
        layers = []
    if secret_volumes is None:
        secret_volumes = {}
    if metadata is None:
        metadata = {}

    new_config_file = ImageConfig(config_file=config_file,
                                  layers=layers,
                                  secret_volumes=secret_volumes,
                                  metadata=metadata,
                                  infinite_command=infinite_command)

    builder = Builder(dockerfile=dockerfile,
                      config=new_config_file,
                      output=Output(*output))
    return builder.build()
