### WORKFLOW with docker-py


1\. initialisation

```python
from docker import Client
client = Client(base_url='unix://var/run/docker.sock')
```

2\. create container from base image

```python
base_container = client.create_container(image="base-image", command='cat -', stdin_open=True)
base_container_id = base_container.get('Id')

client.start(container=base-container_id)
```

3\. run command in first layer

```python
first_command_id = client.exec_create(container=base_container_id, cmd="/usr/share/first-command -a args").get('Id')
client.exec_start(exec_id=first_command_id)

second_command_id = client.exec_create(container=base_container_id, cmd=["/usr/share/second-command", "-a", "args").get('Id')
client.exec_start(exec_id=second_command_id)
```

4\. stop container and commit to new image
```python
client.stop(container=base_container_id)
client.commit(container=base_container_id, repository="middle", tag="l1")
```

5\. create another layer in the same way
```python
middle_container = client.create_container(image="middle:l1", command='cat -, stdin_open=True)
middle_container_id = middle_container.get('Id')

client.start(container=middle-container_id)
```

6\. run commands from another layer
```python
first_command_id = client.exec_create(container=middle_container_id, cmd="/usr/share/another-command -a args").get('Id')
client.exec_start(exec_id=first_command_id)
```

7\. stop middle container and commit to final image
```python
client.stop(container=middle_container_id)
client.commit(container=middle_container_id, repository="final", tag="latest")
```
