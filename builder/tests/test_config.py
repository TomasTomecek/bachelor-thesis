"""
:::::::::::::
!!!LICENCE!!!
:::::::::::::

Tests for Config class.
"""

import jsonschema
import unittest

# local imports
from builder.core.constants import DEFAULT_VERSION
from builder.core.config import ImageConfig


class TestConfig(unittest.TestCase):
    def setUp(self):
        pass

    def test_version(self):

        self.assertEqual(ImageConfig({}).version(), DEFAULT_VERSION,
                         "If no version specified, than there have to be default.")

        ImageConfig({"version": 1.0}).validate()

    def test_layers(self):

        ImageConfig({
            "version": 1.0,
            "layers": [0, 1, 2]
        }).validate()

        self.assertEqual(ImageConfig({
            "version": 1.0,
            "layers": [2, 10, 5]
        }).layers(), [2, 5, 10], "Final layers have to be ordered.")

        self.assertEqual(ImageConfig({
            "version": 1.0,
            "layers": [2, 1, 1, 2, 10, 10, 5]
        }).layers(), [1, 2, 5, 10], "Final layers have to be unique.")

        self.assertEqual(ImageConfig({
            "version": 1.0,
            "layers": [0, 2, 10, 5]
        }).layers(), [2, 5, 10], "Final layers have to be without zero.")

    def test_volume(self):

        ImageConfig({
            "version": 1.0,
            "volumes": {
                "srv/data/test/webapp/secret": "usr/share/webapp/secret"
            }
        }).validate()

        ImageConfig({
            "version": 1.0,
            "volumes": {
                "srv/data/test/webapp/secret": "usr/share/webapp/secret",
                "srv/data/test/webapp2/secret": "usr/share/webapp2/secret"
            }
        }).validate()

        try:
            ImageConfig({"version": 1.0}).validate()
        except jsonschema.ValidationError as e:
            raise jsonschema.ValidationError("Volumes not have to be there.", e)

    def test_metadata(self):

        ImageConfig({
            "version": 1.0,
            "metadata": {
                "name": "test_name"
            }
        }).validate()

        ImageConfig({
            "version": 1.0,
            "metadata": {"name1": "value1",
                         "name2": "value2"
                         }
        }).validate()

        self.assertEqual(
            ImageConfig({
                "version": 1.0,
                "metadata": {
                    "name": "test_name"
                }
            }).metadata(),
            {"name": "test_name"})

        self.assertEqual(
            ImageConfig({
                "version": 1.0,
                "metadata": {
                    "name1": "value1",
                    "name2": "value2"
                }
            }).metadata(),
            {
                "name1": "value1",
                "name2": "value2"
            })

        self.assertEqual(
            ImageConfig({
                "version": 1.0,
                "metadata": {
                    "name1": "value1",
                    "name2": "value2"
                }
            }, metadata={"name2": "value3"}).metadata(),
            {
                "name1": "value1",
                "name2": "value3"
            })

        self.assertEqual(
            ImageConfig({
                "version": 1.0,
                "metadata": {
                    "name1": "value1",
                    "name2": "value2"
                }
            }, metadata={"name3": "value3"}).metadata(),
            {
                "name1": "value1",
                "name2": "value2",
                "name3": "value3"
            })

    def test_infinite_command(self):
        self.assertEqual(
            ImageConfig({
                "infinite_command": "command_1"
            }).infinite_command(),
            "command_1")

        self.assertEqual(
            ImageConfig({}, infinite_command="command_2").infinite_command(),
            "command_2")

        self.assertEqual(
            ImageConfig({
                "infinite_command": "command_1"
            }, infinite_command="command_2").infinite_command(),
            "command_2")

    def test_all(self):
        ImageConfig({
            "version": 1.0,
            "layers": [0, 1, 2],
            "volumes": {
                "/srv/data/test/webapp/secret": "/usr/share/webapp/secret",
                "/srv/data/test/webapp2/secret": "/usr/share/webapp2/secret"
            },
            "metadata": {
                "name1": "value1",
                "name2": "value2"
            },
            "infinite_command": "cat -- "
        }).validate()


if __name__ == '__main__':
    unittest.main()
