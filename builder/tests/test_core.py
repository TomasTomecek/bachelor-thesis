"""
:::::::::::::
!!!LICENCE!!!
:::::::::::::

Test class for core methods.
"""

# python2/3 compatibility
import ast
import io
import tarfile

import re

import docker
import six

import os
import unittest

# local imports
from builder.core.core import Builder
from builder.core.exceptions import DockerClientUnavailable


class TestCore(unittest.TestCase):
    def _establish_client(self):
        """
        Establishes docker client from environment variables.
        """
        self.client = docker.from_env()

    def make_test_on_containers(self, command, dockerfile=None, msg="", context=None):
        command = 'sh -c "{}" '.format(command)

        if dockerfile is None and context is None:
            raise DockerIO("Need context or dockerfile")
            # TODO: better error

        image_orig = self.create_image_orig(dockerfile_content=dockerfile)
        image_builder = self.create_image_builder(dockerfile)
        print("IMAGE: orig: {} builder: {}".format(str(image_orig), str(image_builder)))

        result_builder = self.get_result(image_builder, command)
        result_orig = self.get_result(image_orig, command)
        print("RESULT:\norig: {}builder: {}".format(str(result_orig), str(result_builder)))

        try:
            self.client.images.remove(image_builder.id)
            self.client.images.remove(image_orig.id)
        except:
            pass


        self.assertEqual(result_builder, result_orig, msg)



    def create_image_orig(self, context=None, dockerfile_fileobj=None, dockerfile_content=None):

        if context is None:
            if dockerfile_fileobj is None:
                build_dockerfile_fileobj = io.BytesIO(dockerfile_content.encode('utf-8'))
            else:
                build_dockerfile_fileobj = dockerfile_fileobj

            context_fileobj = io.BytesIO()

            with tarfile.open(mode="w", fileobj=context_fileobj) as tar_file:
                tarinfo = tarfile.TarInfo(name="Dockerfile")
                tarinfo.size = len(build_dockerfile_fileobj.getvalue())
                tar_file.addfile(tarinfo=tarinfo, fileobj=build_dockerfile_fileobj)

            if dockerfile_fileobj is None:
                build_dockerfile_fileobj.close()

            context = context_fileobj.getvalue()
            context_fileobj.close()

        return self.client.images.build(rm=True, fileobj=context,
                                        custom_context=True,
                                        buildargs={})

    def create_image_builder(self, dockerfile, context=None):
        builder = Builder(dockerfile)
        return builder.build()

    def get_result(self, image, command):
        output = self.client.containers.run(image=image, command=command, remove=True)
        return output

    def test_build(self):
        """
        Testing build.
        """
        self.make_test_on_containers(dockerfile="FROM busybox", command=r"id")

    def test_env(self):
        """
        Testing ENV.
        """
        for dockerfile_line, test in [("VARIABLE=something", "echo $VARIABLE"),
                                      ("VAR1=something VAR2=other", "echo $VAR1-$VAR2"),
                                      ('myName="John Doe" myDog=Rex\ The\ Dog \\' + '\nmyCat=fluffy',
                                       "echo $myName-$myDog-$myCat"),
                                      ("ENV myName John Doe\nENV myDog Rex The Dog\nENV myCat fluffy",
                                       "echo $myName-$myDog-$myCat")]:
            self.make_test_on_containers(dockerfile="FROM fedora\nENV {}\n".format(dockerfile_line),
                                         command=test)


    def test_workdir(self):
        for workdir in ["/tmp", "/tmp/not/existing/folder"]:
            self.make_test_on_containers(dockerfile="FROM fedora\nWORKDIR {}\n".format(workdir),
                                     command="echo $PWD")

    def test_user(self):
        self.make_test_on_containers(dockerfile="FROM fedora\nUSER 991\n",
                                     command="id -u")

    def setUp(self):
        self._establish_client()

    def tearDown(self):
        pass


if __name__ == '__main__':
    unittest.main()
