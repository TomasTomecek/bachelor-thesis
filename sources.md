# Sources

 * dockerfile parser: https://github.com/DBuildService/dockerfile-parse

 * new (unmaintained) docker builder by docker: https://github.com/jlhawn/dockramp

 * /build API endpoint: https://docs.docker.com/engine/reference/api/docker_remote_api_v1.22/#build-image-from-a-dockerfile

 * RedHat tool for building images: https://github.com/projectatomic/atomic-reactor

 * community tool for building images: https://github.com/grammarly/rocker

 * RedHat tool for image squashing: https://github.com/goldmann/docker-squash

 * docker-py documentation: https://docker-py.readthedocs.io/en/latest/api/#commit 