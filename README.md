# Bachelor's thesis

### Student: Lachman František
### Leader: Mgr. Marek Grác, Ph.D. (MU, Red Hat, Inc.)
### Technical leader: Mgr. Tomáš Tomeček (Red Hat, Inc.)

----------


[Building docker images without accessing /build API endpoint](https://diplomky.redhat.com/thesis/show/418/building-docker-images-without-accessing-build-api-endpoint)

Text of the thesis is created with [gitbook](https://lachmanfrantisek.gitbooks.io/bachelor-thesis-text). Web version can be seen on [lachmanfrantisek.gitbooks.io/bachelor-thesis-text/content](https://lachmanfrantisek.gitbooks.io/bachelor-thesis-text/content).
