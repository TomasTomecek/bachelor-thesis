# API of builder

## Use-cases

### Context sources:

- git repository
- local directory
- archive

### Dockerfile sources:
- path in context
- string
- file object

## 1. compatible version of core method for building images - `build`

Use same parameters as [docker-py](http://docker-py.readthedocs.io/en/stable/images.html#docker.models.images.ImageCollection.build), but extended.


### Parameters:
- `path` (str) -- Path to directory or url to repository used as a build context.
- `fileobj` -- A file object used as the Dockerfile. (If `custom_context=True`, than archive with build context.)
- `dockerfile` (str) -- Path within the build context to the dockerfile.
- `dockerfile_content` (str) -- String representain of dockerfile.
- `custom_context` (bool) -- If `True`, `fileobj` is used as a build context in archive, else it is only dockerfile (default `False`).


## Examples

### Context sources:

- git repository

```python
# Dockerfile has to be '/Dockerfile' in repository.
image = build(path="https://my.git.repository")

# Set own location of dockerfile in context.
image = build(path="https://my.git.repository", dockerfile="/dockerfiles/RepoDockerFile")

# Use external dockerfile.
image = build(path="https://my.git.repository", fileobj=my_dockerfile_fileobj)
# or
image = build(path="https://my.git.repository", dockerfile_content="from busybox:latest")
```

- local directory

```python
# Dockerfile has to be './Dockerfile' in context directory.
image = build(path="/data/context")

# Set own location of dockerfile in context.
# - path in context: /dockerfiles/RepoDockerFile
# - path in host: /data/context/dockerfiles/RepoDockerFile
image = build(path="/data/context", dockerfile="/dockerfiles/RepoDockerFile")

# Use external dockerfile.
image = build(path="/data/context", fileobj=my_dockerfile_fileobj)
# or
image = build(path="/data/context", dockerfile_content="from busybox:latest")
```


- archive

```python
# Dockerfile has to be './Dockerfile' in archive.
image = build(fileobj=my_tar_archive_context, custom_context=True)

# Set own location of dockerfile in context.
image = build(fileobj=my_tar_archive_context, custom_context=True, dockerfile="/dockerfiles/RepoDockerFile")

# Use external dockerfile.
image = build(fileobj=my_tar_archive_context, custom_context=True, dockerfile_content="from busybox:latest")
```


## 2.1. uncompatible version of core method for building images - `build` (short)
### Parameters:
- `context` -- Path to directory, url to repository or fileobj used as a build context.
- `dockerfile` -- Path to Dockerfile in context, or fileobj/string representation of dockerfile.




## Examples

### Context sources:

- git repository

```python
# Dockerfile has to be '/Dockerfile' in repository.
image = build(context="https://my.git.repository")

# Set own location of dockerfile in context.
image = build(context="https://my.git.repository", dockerfile="/dockerfiles/RepoDockerFile")

# Use external dockerfile.
image = build(context="https://my.git.repository", dockerfile=my_dockerfile_fileobj)
# or
image = build(context="https://my.git.repository", dockerfile="from busybox:latest")
```

- local directory

```python
# Dockerfile has to be './Dockerfile' in context directory.
image = build(context="/data/context")

# Set own location of dockerfile in context.
# - path in context: /dockerfiles/RepoDockerFile
# - path in host: /data/context/dockerfiles/RepoDockerFile
image = build(context="/data/context", dockerfile="/dockerfiles/RepoDockerFile")

# Use external dockerfile.
image = build(context="/data/context", dockerfile=my_dockerfile_fileobj)
# or
image = build(context="/data/context", dockerfile="from busybox:latest")
```


- archive

```python
# Dockerfile has to be './Dockerfile' in archive.
image = build(fileobj=my_tar_archive_context, custom_context=True)

# Set own location of dockerfile in context.
image = build(fileobj=my_tar_archive_context, dockerfile="/dockerfiles/RepoDockerFile")

# Use external dockerfile.
image = build(fileobj=my_tar_archive_context, dockerfile=my_dockerfile_fileobj)
# or
image = build(fileobj=my_tar_archive_context, dockerfile="from busybox:latest")
```

## 2.2. uncompatible version of core method for building images - `build` (long)

### Parameters:
- `context_path` (str) -- Parh or url to build context.
- `context_fileobj` --  Archive containing build context.
- `dockerfile_path` (str) -- Path to dockerfile in build context.
- `dockerfile_fileobj` -- Fileobj with dockerfile.
- `dockerfile_content` -- Content of dockerfile as a string.

## Examples

### Context sources:

- git repository

```python
# Dockerfile has to be '/Dockerfile' in repository.
image = build(context_path="https://my.git.repository")

# Set own location of dockerfile in context.
image = build(context_path="https://my.git.repository", dockerfile_path="/dockerfiles/RepoDockerFile")

# Use external dockerfile.
image = build(context_path="https://my.git.repository", dockerfile_fileobj=my_dockerfile_fileobj)
# or
image = build(context_path="https://my.git.repository", dockerfile_content="from busybox:latest")
```

- local directory

```python
# Dockerfile has to be './Dockerfile' in context directory.
image = build(context_path="/data/context")

# Set own location of dockerfile in context.
# - path in context: /dockerfiles/RepoDockerFile
# - path in host: /data/context/dockerfiles/RepoDockerFile
image = build(context_path="/data/context", dockerfile_path="/dockerfiles/RepoDockerFile")

# Use external dockerfile.
image = build(context_path="/data/context", dockerfile_fileobj=my_dockerfile_fileobj)
# or
image = build(context_path="/data/context", dockerfile_content="from busybox:latest")
```


- archive

```python
# Dockerfile has to be './Dockerfile' in archive.
image = build(context_fileobj=my_tar_archive_context)

# Set own location of dockerfile in context.
image = build(context_fileobj=my_tar_archive_context, dockerfile_path="/dockerfiles/RepoDockerFile")

# Use external dockerfile.
image = build(context_fileobj=my_tar_archive_context, dockerfile_fileboj=my_dockerfile_fileobj)
# or
image = build(context_fileobj=my_tar_archive_context, dockerfile_content="from busybox:latest")
```